import React, { Component } from "react";
import {
  createStackNavigator,
  createAppContainer,
  createBottomTabNavigator
} from "react-navigation";
import { observer } from "mobx-react";
import { Icon } from "native-base";

import HomeScreen from "./src/components/containers/HomeScreen";
import DetailsScreen from "./src/components/containers/DetailsScreen";
import UserScreen from "./src/components/containers/UserScreen";
import appStore from "./src/model/mobx/ApplicationStore";
import SettingScreen from "./src/components/containers/SettingScreen";
import TabBar from "./src/components/views/TabBar"
import { themeConstants } from "./src/res/themeConstants";
import { cache } from "./src/utils/Cache";

const AppNavigator = createBottomTabNavigator(
  {
    HomeScreen: {
      screen: HomeScreen,
      navigationOptions: {
        tabBarLabel: "Home",
        tabBarIcon: ({ tintColor }: any) => (
          <Icon name="home" style={{ color: `${tintColor}` }} />
        )
      }
    },
    DetailsScreen: {
      screen: DetailsScreen,
      navigationOptions: {
        tabBarLabel: "Details",
        tabBarIcon: ({ tintColor }: any) => (
          <Icon name="image" style={{ color: `${tintColor}` }} />
        )
      }
    },
    UserScreen: {
      screen: UserScreen,
      navigationOptions: {
        tabBarLabel: "User",
        tabBarIcon: ({ tintColor }: any) => (
          <Icon name="person" style={{ color: `${tintColor}` }} />
        )
      }
    },
    SettingScreen: {
      screen: SettingScreen,
      navigationOptions: {
        tabBarLabel: "Settings",
        tabBarIcon: ({tintColor}: any) => (
          <Icon name="settings" style={{ color: `${tintColor}` }} />
        )
      }
    }
  },
  {
    tabBarComponent: TabBar
  }
);

const AppContainer = createAppContainer(AppNavigator);

export const ThemeContext = React.createContext('light');

@observer
export default class App extends Component {

  componentDidMount() {
    appStore.getThemeFromCache()
  }

  render() {
    return (
      <ThemeContext.Provider value={appStore.themeValue}>
        <AppContainer />
      </ThemeContext.Provider>
    );
  }
}
