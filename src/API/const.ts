export enum MainParameters {
  URL = "https://www.flickr.com/services/rest/?",
  API_KEY = "api_key=212320d7e81e8942a1d34ecc0699a164",
  FORMAT = "format=json&nojsoncallback=1"
}

export enum ApiMethods {
  PHOTOS_SEARCH = "method=flickr.photos.search&",
  PEOPLE_GET_PHOTOS = "method=flickr.people.getPhotos&",
  FIND_BY_USERNAME = "method=flickr.people.findByUsername&",
  GET_COMMENTS = "method=flickr.photos.comments.getList&",
  GET_RECENT = "method=flickr.photos.getRecent&",
}
