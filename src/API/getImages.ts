import { getRequest } from "./services/FlickrService";
import { MainParameters, ApiMethods } from "./const";

export const getImagesByTag = async (tag: string = "", page: number = 1) => {
  const responseJson = await getRequest(
    `${MainParameters.URL}${ApiMethods.PHOTOS_SEARCH}${MainParameters.API_KEY}&tags=${tag}&tag_mode=all&page=${page}&per_page=20&extras=tags%2C+date_taken%2C+views%2C+%2C+owner_name%2C+url_m%2C+description&${MainParameters.FORMAT}`
  );
  return responseJson.photos.photo;
};

export const getImagesByUser = async (userID: string, page: number = 1) => {
  const responseJson = await getRequest(
    `${MainParameters.URL}${ApiMethods.PEOPLE_GET_PHOTOS}${MainParameters.API_KEY}&user_id=${userID}&${MainParameters.FORMAT}&page=${page}&per_page=20&extras=tags%2C+views%2C+date_taken%2C+owner_name%2C+url_m%2C+description`
  );
  return responseJson.photos.photo;
};

export const getUserByName = async (name: string) => {
  const responseJson = await getRequest(
    `${MainParameters.URL}${ApiMethods.FIND_BY_USERNAME}${MainParameters.API_KEY}&username=${name}&${MainParameters.FORMAT}`
  )
  console.log(responseJson)
  return responseJson.user
}

export const getRecentImages = async (page: number = 1) => {
  const responseJson = await getRequest(
    `${MainParameters.URL}${ApiMethods.GET_RECENT}${MainParameters.API_KEY}&page=${page}&per_page=20&extras=tags%2C+date_taken%2C+views%2C+%2C+owner_name%2C+url_m%2C+description&${MainParameters.FORMAT}`
  )
  return responseJson.photos.photo
}

export const getCommentsByPhoto = async (id: string) => {
  const responseJson = await getRequest(
    `${MainParameters.URL}${ApiMethods.GET_COMMENTS}${MainParameters.API_KEY}&photo_id=${id}&${MainParameters.FORMAT}&page=1&per_page=10`
  )
  console.log(responseJson)
  return responseJson.comments.comment
}
