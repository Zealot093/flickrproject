import { Alert } from "react-native";

export const getRequest = async (request: string) => {
  try {
    let response = await fetch(request);
    let responseJson = await response.json();
    return responseJson;
  } catch (error) {
    console.log(error);
    Alert.alert(error.message);
  }
};

export const urlAssembl = () => {
  
}