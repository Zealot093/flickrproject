import React, { PureComponent } from "react";
import {
  Text,
  StyleSheet,
  Dimensions,
  ScrollView,
  Button,
  Animated
} from "react-native";
import { Container } from "native-base";
import { observer } from "mobx-react";

import DetailsHeader from "../views/DetailsHeader";
import ModalImage from "../views/ModalImage";
import UserName from "../views/UserName";
import PhotoDetails from "../views/PhotoDetails";
import { WELCOME_TEXT } from "../../res/strings";
import { downloadFile } from "../../utils/downloadPhoto";
import store from "../../model/mobx/Store";
import { FlatList } from "react-native-gesture-handler";
import CommentList from "../views/CommentList";
import { goToUser } from "../../utils/goToUser";
import { ThemeContext } from "../../../App";
import { themeConstants } from "../../res/themeConstants";

interface Props {
  navigation: any;
}
@observer
class Details extends PureComponent<Props> {
  state = {
    modalVisible: false
  };

  setModalVisible(visible: boolean) {
    this.setState({ modalVisible: visible });
  }

  render() {
    const item = this.props.navigation.getParam("item");

    return (
      <ThemeContext.Consumer>
        {theme => (
          <Container style={{backgroundColor: themeConstants[theme].SECOND_BACKGROUND_COLOR}}>
            <DetailsHeader
              navigation={this.props.navigation}
              title={item ? item.ownername : "Details"}
            />
            {item ? (
              <ScrollView>
                <UserName
                  name={item.ownername}
                  userId={item.owner}
                  navigation={this.props.navigation}
                  onPressName={() =>
                    goToUser(item.owner, item.ownername, this.props.navigation)
                  }
                />
                <ModalImage
                  item={item}
                  setModalVisible={() =>
                    this.setModalVisible(!this.state.modalVisible)
                  }
                  modalVisible={this.state.modalVisible}
                  downloadPhoto={() => downloadFile(item.url_m)}
                />
                <PhotoDetails item={item} />
                <Text style={[styles.description, {color: themeConstants[theme].SECOND_TEXT_COLOR}]}>
                  {item.description._content}
                </Text>
                <CommentList item={item} navigation={this.props.navigation} />
              </ScrollView>
            ) : (
              <Text style={styles.firstText}>{WELCOME_TEXT}</Text>
            )}
          </Container>
        )}
      </ThemeContext.Consumer>
    );
  }
}

const styles = StyleSheet.create({
  image: {
    width: Dimensions.get("window").width,
    height: Dimensions.get("window").width
  },
  firstText: {
    height: 500,
    textAlignVertical: "center",
    textAlign: "center"
  },
  description: {
    margin: 12
  }
});

export default Details;
