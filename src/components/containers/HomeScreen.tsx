import React, { PureComponent } from "react";
import { Text, StyleSheet, ActivityIndicator } from "react-native";
import { observer } from "mobx-react";
import { Container } from "native-base";

import store from "../../model/mobx/Store";
import ImageList from "../views/ImageList";
import { WELCOME_TEXT } from "../../res/strings";
import SearchHeader from "../views/SearchHeader";
import TagsHistory from "../views/TagsHistory";
import { ThemeContext } from "../../../App";
import { toJS } from "mobx";
import { themeConstants } from "../../res/themeConstants";

interface Props {
  navigation: any;
}
@observer
class HomeScreen extends PureComponent<Props> {
  state = {
    columns: 1
  };

  componentDidMount() {
    store.getTagsFromCache();
    store.searchRecentPhoto();
  }

  changeColumns = () => {
    this.state.columns === 1
      ? this.setState({ columns: 2 })
      : this.setState({ columns: 1 });
  };

  onPressTag = (tag: string) => {
    store.loading = true;
    store.searchText = tag;
    store.searchPhotoByTag();
  };

  render() {
    console.log("HomeScreen render");
    let text;
    if (store.images.length === 0) {
      text = <Text style={styles.firstText}>{WELCOME_TEXT}</Text>;
    }
    return (
      <ThemeContext.Consumer>
        {theme => {
          console.log(toJS(theme));
          return (
            <Container
              style={[
                styles.container,
                { backgroundColor: themeConstants[theme].MAIN_BACKGROUND_COLOR }
              ]}
            >
              <SearchHeader
                changeColumns={this.changeColumns}
                columns={this.state.columns}
              />
              <TagsHistory
                tags={store.historyTags}
                onPressTag={this.onPressTag}
              />

              <ImageList
                navigation={this.props.navigation}
                columns={this.state.columns}
                images={store.uniqueImages}
                handleLoadMore={store.handleLoadMore}
              />
            </Container>
          );
        }}
      </ThemeContext.Consumer>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  firstText: {
    height: 500,
    textAlignVertical: "center",
    textAlign: "center"
  }
});

export default HomeScreen;
