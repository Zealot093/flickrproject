import React, { PureComponent } from "react";
import { View, Text, StyleSheet } from "react-native";

import DetailsHeader from "../views/DetailsHeader";
import {
  Container,
  Content,
  ListItem,
  Left,
  Button,
  Icon,
  Body,
  Right,
  Switch
} from "native-base";
import SettingsList from "../views/SettingsList";
import appStore from "../../model/mobx/ApplicationStore";
import { ThemeContext } from "../../../App";
import { observer } from "mobx-react";
import { themeConstants } from "../../res/themeConstants";

interface Props {
  navigation: any;
}
@observer
class SettingScreen extends PureComponent<Props> {
  render() {
    return (
      <ThemeContext.Consumer>
        {theme => (
          <Container style={{backgroundColor: themeConstants[theme].SECOND_BACKGROUND_COLOR}}>
            <DetailsHeader
              navigation={this.props.navigation}
              title="Settings"
            />
            <SettingsList changeTheme={appStore.changeTheme} nightThemeSwitch={appStore.nightThemeSwitch} />
          </Container>
        )}
      </ThemeContext.Consumer>
    );
  }
}

export default SettingScreen;
