import React, { PureComponent } from "react";
import { View, Text, StyleSheet } from "react-native";
import { Container } from "native-base";
import { observer } from "mobx-react";

import ImageList from "../views/ImageList";
import store from "../../model/mobx/Store";
import DetailsHeader from "../views/DetailsHeader";
import UserName from "../views/UserName";
import { WELCOME_TEXT } from "../../res/strings";
import UserHeader from "../views/UserHeader";
import { toJS } from "mobx";
import { ThemeContext } from "../../../App";
import { themeConstants } from "../../res/themeConstants";

interface Props {
  navigation: any;
}
@observer
class UserScreen extends PureComponent<Props> {
  render() {
    console.log(store.user);
    console.log(toJS(store.userImages));

    return (
      <ThemeContext.Consumer>
        {theme => (
          <Container style={{backgroundColor: themeConstants[theme].SECOND_BACKGROUND_COLOR}}>
            <UserHeader />
            {store.userImages.length != 0 ? (
              <View style={{ flex: 1 }}>
                <UserName
                  name={store.user.username._content}
                  userId={store.user.id}
                />
                <ImageList
                  navigation={this.props.navigation}
                  columns={2}
                  images={store.userImages}
                  handleLoadMore={() =>
                    store.handleLoadMoreUserPhoto(store.user.id)
                  }
                />
              </View>
            ) : (
              <Text style={styles.firstText}>Найдите пользователя!</Text>
            )}
          </Container>
        )}
      </ThemeContext.Consumer>
    );
  }
}

const styles = StyleSheet.create({
  firstText: {
    height: 500,
    textAlignVertical: "center",
    textAlign: "center"
  }
});

export default UserScreen;
