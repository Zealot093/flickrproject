import React, { PureComponent } from "react";
import { StyleSheet, TouchableOpacity, View, Dimensions } from "react-native";
import { observer } from "mobx-react";
import FastImage from "react-native-fast-image";

import store from "../../model/mobx/Store";
import UserName from "./UserName";
import PhotoDetails from "./PhotoDetails";
import { iImage } from "../../model/interfaces";
import { goToUser } from "../../utils/goToUser";
import { ThemeContext } from "../../../App";
import { toJS } from "mobx";
import { themeConstants } from "../../res/themeConstants";

const DOUBLECLICK_TIMEOUT = 50;

interface Props {
  navigation: any;
  columns: number;
  item: iImage;
  onImageClick?(): void;
}
@observer
export default class CardImageItem extends PureComponent<Props> {
  pressCount: number = 0;

  handleImagePress = () => {
    this.pressCount = this.pressCount + 1;

    setTimeout(() => {
      if (this.pressCount == 0) {
        return;
      }

      if (this.pressCount == 1) {
        console.log("single tap");
        store.comments = [];
        this.props.navigation.navigate("DetailsScreen", {
          item: this.props.item
        });
      } else if (this.pressCount == 2) {
        console.log("double tap");
        let arrayTags = this.props.item.tags.split(" ", 5).toString();
        store.searchText = arrayTags;
        store.searchPhotoByTag();
      }

      this.pressCount = 0;
    }, DOUBLECLICK_TIMEOUT);
  };

  goToUser = () => {
    store.searchPhotoByUser(this.props.item.owner);
    this.props.navigation.navigate("UserScreen", {
      item: this.props.item
    });
  };

  render() {
    return (
      <ThemeContext.Consumer>
        {theme => {
          let Bottom, Top;
          if (this.props.columns === 1) {
            Top = (
              <UserName
                name={this.props.item.ownername}
                userId={this.props.item.owner}
                navigation={this.props.navigation}
                onPressName={() =>
                  goToUser(
                    this.props.item.owner,
                    this.props.item.ownername,
                    this.props.navigation
                  )
                }
              />
            );

            Bottom = <PhotoDetails item={this.props.item} />;
          }
          return (
            <View
              style={[
                styles.card,
                { backgroundColor: themeConstants[theme].SECOND_BACKGROUND_COLOR }
              ]}
            >
              {Top}
              <View style={{ backgroundColor: themeConstants[theme].MAIN_BACKGROUND_COLOR }}>
                <TouchableOpacity onPress={this.handleImagePress}>
                  <FastImage
                    style={
                      this.props.columns === 1
                        ? {
                            width: Dimensions.get("window").width - 12,
                            height: Dimensions.get("window").width - 12
                          }
                        : {
                            width: Dimensions.get("window").width / 2 - 9,
                            height: Dimensions.get("window").width / 2 - 9,
                            borderRadius: 5
                          }
                    }
                    source={{ uri: this.props.item.url_m }}
                    resizeMode="cover"
                  />
                </TouchableOpacity>
              </View>
              {Bottom}
            </View>
          );
        }}
      </ThemeContext.Consumer>
    );
  }
}

const styles = StyleSheet.create({
  card: {
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowOpacity: 0.23,
    shadowRadius: 2.62,
    margin: 3,
    marginTop: 6,
    elevation: 4,
    borderRadius: 5
  },
  text: {
    fontSize: 15
  }
});
