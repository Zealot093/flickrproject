import React, { PureComponent } from "react";
import { StyleSheet, View, Text } from "react-native";
import { Thumbnail } from "native-base";

import { urlAvatarAssemble } from "../../utils/urlAvatarAssemble";
import store from "../../model/mobx/Store";
import { iComment } from "../../model/interfaces";
import { goToUser } from "../../utils/goToUser";
import { ThemeContext } from "../../../App";
import { themeConstants } from "../../res/themeConstants";

interface Props {
  navigation: any;
  comment: iComment;
}
class Comment extends PureComponent<Props> {
  render() {
    return (
      <ThemeContext.Consumer>
        {theme => (
          <View style={styles.container}>
            <Thumbnail
              source={{ uri: urlAvatarAssemble(this.props.comment.author) }}
            />
            <View style={styles.userData}>
              <Text
                style={[styles.userName, {color: themeConstants[theme].MAIN_TEXT_COLOR}]}
                onPress={() =>
                  goToUser(
                    this.props.comment.author,
                    this.props.comment.authorname,
                    this.props.navigation
                  )
                }
              >
                {this.props.comment.authorname}
              </Text>
              <Text style={{color: themeConstants[theme].SECOND_TEXT_COLOR}}>{this.props.comment._content}</Text>
            </View>
          </View>
        )}
      </ThemeContext.Consumer>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flexDirection: "row",
    margin: 12
  },
  userData: {
    marginTop: 6,
    marginRight: 50,
    marginLeft: 10
  },
  userID: {
    fontSize: 12,
    color: "#808080"
  },
  userName: {
    color: "black"
  }
});

export default Comment;
