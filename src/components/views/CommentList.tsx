import React, { PureComponent } from "react";
import { View, FlatList, Text } from "react-native";
import { observer } from "mobx-react";
import store from "../../model/mobx/Store";
import { iImage } from "../../model/interfaces";
import Comment from "./Comment";
import { ThemeContext } from "../../../App";
import { themeConstants } from "../../res/themeConstants";

interface Props {
  navigation: any;
  item: iImage;
}
@observer
class CommentList extends PureComponent<Props> {
  renderItem = ({ item }: any) => (
    <Comment comment={item} navigation={this.props.navigation} />
  );

  render() {
    return (
      <ThemeContext.Consumer>
        {theme => (
          <View>
            <Text
              style={{ textAlign: "center", margin: 20, color: themeConstants[theme].MAIN_TEXT_COLOR }}
              onPress={() => store.getComments(this.props.item.id)}
            >
              Show comments
            </Text>
            <FlatList
              data={store.comments}
              keyExtractor={item => item.id}
              renderItem={this.renderItem}
            />
          </View>
        )}
      </ThemeContext.Consumer>
    );
  }
}

export default CommentList;
