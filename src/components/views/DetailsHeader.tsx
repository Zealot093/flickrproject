import React, { PureComponent } from "react";
import { Header, Icon, Button, Left, Body, Title } from "native-base";

interface Props {
  navigation: any;
  title: string;
}
export default class SearchBar extends PureComponent<Props> {
  render() {
    return (
      <Header androidStatusBarColor="#181818" style={{backgroundColor: "#262629"}}>
        <Left>
          <Button
            transparent
            onPress={() => this.props.navigation.navigate("HomeScreen")}
          >
            <Icon name="arrow-back" />
          </Button>
        </Left>
        <Body>
          <Title>{this.props.title}</Title>
        </Body>
      </Header>
    );
  }
}
