import React, {PureComponent} from 'react';
import { View, Text, StyleSheet, TextInput } from 'react-native';
import { observer } from 'mobx-react';
import store from '../../model/mobx/Store';

@observer
class Header extends PureComponent {

    onSubmitEditing = (text: string) => {

    }

    handleText = (text: string) => {

    }

    render() {
        return(
            <View style={styles.container}>
                <View  style={styles.title}><Text>Header here!</Text></View>
                <View  style={styles.search}>
                    <TextInput
                        placeholder={'search by tag'}
                        onChangeText={store.handleSearchText}
                        onSubmitEditing={store.searchPhotoByTag}/>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'row',
        //height: 56,
        marginBottom: 20,
        backgroundColor: '#ffffff',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.22,
        shadowRadius: 2.22,

        elevation: 3,
        },
    title: {
        width: 100,
        height: 100,
    },
    search: {
        width: 100,
        height: 100
    }    
})

export default Header