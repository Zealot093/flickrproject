import React, { PureComponent } from "react";
import {
  View,
  FlatList,
  StyleSheet,
  ActivityIndicator,
  ScrollView
} from "react-native";
import { observer } from "mobx-react";

import CardImageItem from "./CardItem";
import { iImage } from "../../model/interfaces";
import { ThemeContext } from "../../../App";
import { toJS } from "mobx";
import { themeConstants } from "../../res/themeConstants";

interface Props {
  columns: number;
  navigation: any;
  images: iImage[];
  handleLoadMore?(): void;
}

@observer
class ImageList extends PureComponent<Props> {
  renderItem = ({ item }: any) => (
    <CardImageItem
      navigation={this.props.navigation}
      columns={this.props.columns}
      item={item}
    />
  );

  renderFooterComponent = () => (
    <ActivityIndicator size="small" animating={true} />
  );

  render() {
    console.log("ImageList render");
    return (
      <ThemeContext.Consumer>
        {theme => {
          console.log(toJS(theme))
          return (
            <View
              style={[
                styles.container,
                { backgroundColor: themeConstants[theme].MAIN_BACKGROUND_COLOR }
              ]}
            >
              <FlatList
                data={this.props.images.slice()}
                renderItem={this.renderItem}
                keyExtractor={item => item.secret}
                numColumns={this.props.columns}
                key={this.props.columns}
                onEndReachedThreshold={4}
                onEndReached={this.props.handleLoadMore}
                ListFooterComponent={this.renderFooterComponent}
              />
            </View>
          );
        }}
      </ThemeContext.Consumer>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: 3
  }
});

export default ImageList;
