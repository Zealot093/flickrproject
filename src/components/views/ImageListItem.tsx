import React, {PureComponent} from 'react';
import { View, TouchableOpacity, Text, Image, StyleSheet } from 'react-native';
import store from '../../model/mobx/Store';
import { urlAvatarAssemble } from '../../utils/urlAvatarAssemble';

interface Props {
    navigation: any
    columns: number
    item: {
        url_m: string
        title: string
        owner: string
        ownername: string
        datetaken: string
        description: {_content: string}
        tags: string
    }
}
class ImageListItem extends PureComponent<Props> {

    render() {
        let Bottom, Top
        if (this.props.columns === 1) {
            Top =   <View style={{marginTop: 10}}>
                            <Image style={{width: 30, height: 30}}
                            source={{uri: urlAvatarAssemble(this.props.item.owner)}}/>
                            <Text>Ownername: {this.props.item.ownername}</Text>
                        </View> 

            Bottom =    <View style={{marginTop: 5, paddingBottom: 10, borderBottomWidth: 1, borderBottomColor: '#a8a8a8'}}>
                                <Text>Title: {this.props.item.title}</Text>
                                <Text>Date: {this.props.item.datetaken}</Text>
                            </View>
        }
        return(
            <View style={styles.container}>
                {Top}
                <TouchableOpacity onPress={() => this.props.navigation.navigate('DetailsScreen',
                    {
                        url: this.props.item.url_m,
                        title: this.props.item.title,
                        ownername: this.props.item.ownername,
                        date: this.props.item.datetaken,
                        description: this.props.item.description._content,
                        owner: this.props.item.owner,
                        tags: this.props.item.tags.split(' ')
                    })}>
                <Image 
                    style={
                    this.props.columns === 1 ? {width: 400, height: 400} : {width: 200, height: 200}}
                    source={{uri: this.props.item.url_m}}/>
                </TouchableOpacity>
                {Bottom}
            </View> 
        )
    }

}

const styles = StyleSheet.create({
    container: {
        
    }
})

export default ImageListItem

