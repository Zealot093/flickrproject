import React, { PureComponent } from "react";
import {
  View,
  Modal,
  TouchableOpacity,
  StyleSheet,
  Dimensions,
} from "react-native";
import { observer } from "mobx-react";
import ImageViewer from "react-native-image-zoom-viewer";
import FastImage from "react-native-fast-image";
import Icon from "react-native-vector-icons/Ionicons";

import { iImage } from "../../model/interfaces";

interface Props {
  setModalVisible(): any;
  downloadPhoto?(): void
  modalVisible: boolean;
  item: iImage;
}
@observer
class ModalImage extends PureComponent<Props> {
  render() {
    return (
      <View>
        <Modal visible={this.props.modalVisible} transparent={true}>
          <ImageViewer
            imageUrls={[{ url: this.props.item.url_m }]}
            enableImageZoom={true}
            enableSwipeDown={true}
            onSwipeDown={this.props.setModalVisible}
          />
        </Modal>
        <TouchableOpacity onPress={this.props.setModalVisible}>
          <FastImage
            style={styles.image}
            source={{ uri: this.props.item.url_m }}
          />
        </TouchableOpacity>
        <Icon
          name="md-arrow-down"
          size={30}
          style={styles.iconDownload}
          onPress={this.props.downloadPhoto}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  image: {
    width: Dimensions.get("window").width,
    height: Dimensions.get("window").width
  },
  iconDownload: {
    marginLeft: 10,
    marginTop: 10
  }
});

export default ModalImage;
