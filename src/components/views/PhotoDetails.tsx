import React, { PureComponent } from "react";
import { View, Text, StyleSheet } from "react-native";
import { observer } from "mobx-react";
import Icon from "react-native-vector-icons/Ionicons";

import TagList from "./TagList";
import { iImage } from "../../model/interfaces";
import { ThemeContext } from "../../../App";
import { themeConstants } from "../../res/themeConstants";

interface Props {
  item: iImage;
}
@observer
class PhotoDetails extends PureComponent<Props> {
  render() {
    return (
      <ThemeContext.Consumer>
        {theme => (
          <View style={[styles.container, {backgroundColor: themeConstants[theme].SECOND_BACKGROUND_COLOR}]}>
            <Text style={{color: themeConstants[theme].MAIN_TEXT_COLOR}}>{this.props.item.title}</Text>
            <Text style={[styles.date, {color: themeConstants[theme].SECOND_TEXT_COLOR}]}>{this.props.item.datetaken}</Text>
            <View style={{ flex: 1, flexDirection: "row" }}>
              <Icon name="md-eye" style={{ marginTop: 4, color: themeConstants[theme].SECOND_TEXT_COLOR }} />
              <Text style={{ marginLeft: 6, color: themeConstants[theme].SECOND_TEXT_COLOR }}>{this.props.item.views}</Text>
            </View>
            <TagList data={this.props.item.tags} />
          </View>
        )}
      </ThemeContext.Consumer>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    margin: 12
  },
  date: {
    fontSize: 12
  },
  title: {
    color: "black"
  }
});

export default PhotoDetails;
