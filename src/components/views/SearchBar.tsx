import React, { PureComponent } from "react";
import { StyleSheet, View } from "react-native";
import { Header, Item, Input, Button, Right, Container } from "native-base";
import Icon from "react-native-vector-icons/FontAwesome";
import { observer } from "mobx-react";

import store from "../../model/mobx/Store";

interface Props {
  changeColumns?(): void;
  columns: number;
}
@observer
class SearchBar extends PureComponent<Props> {
  render() {
    return (
      <Header
        searchBar
        rounded
        hasSegment
        androidStatusBarColor="#181818"
        style={{ backgroundColor: "#262629" }}
      >
        <Item>
          <Icon name="search" size={20} style={styles.iconSearch} />
          <Input
            placeholder="Search by tag"
            onChangeText={store.handleSearchText}
            value={store.searchText}
            onSubmitEditing={store.searchPhotoByTag}
          />
        </Item>
        <Right>
          <Button transparent onPress={this.props.changeColumns}>
            {this.props.columns === 2 ? (
              <Icon name="th-large" size={20} color="#fff" />
            ) : (
              <Icon name="window-maximize" size={20} color="#fff" />
            )}
          </Button>
        </Right>
      </Header>
    );
  }
}

const styles = StyleSheet.create({
  iconSearch: {
    marginHorizontal: 10
  }
});

export default SearchBar;
