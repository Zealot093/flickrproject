import React, { PureComponent } from "react";
import {
  StyleSheet,
  View,
  TextInput,
  Dimensions,
  StatusBar
} from "react-native";
import { Button } from "native-base";
import Icon from "react-native-vector-icons/FontAwesome";
import { observer } from "mobx-react";

import store from "../../model/mobx/Store";
import { MAIN_APP_COLOR, STATUS_BAR_COLOR } from "../../res/colors";

interface Props {
  changeColumns?(): void;
  columns: number;
}
@observer
class SearchHeader extends PureComponent<Props> {
  state = {
    modalVisible: false
  };

  _onSubmit = () => {
    store.loading = true;
    store.searchPhotoByTag()
  }

  render() {
    return (
      <View style={styles.container}>
        <StatusBar backgroundColor={STATUS_BAR_COLOR} />
        <View style={{ flexDirection: "column" }}>
          <View style={styles.search}>
            <Icon name="search" size={20} style={styles.iconSearch} />
            <TextInput
              placeholder="Search by tag"
              placeholderTextColor="#575757"
              onChangeText={store.handleSearchText}
              value={store.searchText}
              onSubmitEditing={this._onSubmit}
              style={styles.searchField}
            />
          </View>
        </View>
        <Button
          transparent
          onPress={this.props.changeColumns}
          style={styles.columnButton}
        >
          {this.props.columns === 2 ? (
            <Icon name="th-large" size={20} color="#fff" />
          ) : (
            <Icon name="window-maximize" size={20} color="#fff" />
          )}
        </Button>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flexDirection: "row",
    height: 56,
    backgroundColor: MAIN_APP_COLOR,
    justifyContent: "space-between",
    paddingHorizontal: 12
  },
  search: {
    backgroundColor: "#fff",
    marginVertical: 8,
    borderRadius: 3,
    flexDirection: "row"
  },
  searchField: {
    height: 40,
    width: Dimensions.get("window").width - 120,
    fontSize: 17
  },
  iconSearch: {
    marginHorizontal: 10,
    textAlignVertical: "center"
  },
  columnButton: {
    marginVertical: 5
  }
});

export default SearchHeader;
