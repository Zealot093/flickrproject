import React, { PureComponent } from "react";
import { View, Text, StyleSheet, Switch } from "react-native";

import DetailsHeader from "../views/DetailsHeader";
import { Container, Content, ListItem, Left, Button, Icon, Body, Right } from "native-base";
import appStore from "../../model/mobx/ApplicationStore";
import { observer } from "mobx-react";

interface Props {
  changeTheme?(): void
  nightThemeSwitch: boolean
}
@observer
class SettingsList extends PureComponent<Props> {
  render() {
    return (
        <Content>
          <ListItem icon>
            <Left>
              <Button style={{ backgroundColor: "#FF9501" }}>
                <Icon active name="brush" />
              </Button>
            </Left>
            <Body>
              <Text>Night Theme</Text>
            </Body>
            <Right>
              <Switch value={this.props.nightThemeSwitch} onValueChange={this.props.changeTheme}/>
            </Right>
          </ListItem>
        </Content>
    );
  }
}

export default SettingsList;