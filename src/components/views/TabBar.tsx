import React, { Component } from "react";
import { BottomTabBar } from "react-navigation";
import { ThemeContext } from "../../../App";
import appStore from "../../model/mobx/ApplicationStore";
import StackNavigator from "react-navigation";
import { themeConstants } from "../../res/themeConstants";

interface Props {}
export default class TabBar extends Component<Props> {
  render() {
    return (
      <ThemeContext.Consumer>
        {theme => (
          <BottomTabBar
            {...this.props}
            showLabel={false}
            activeTintColor={themeConstants[theme].TAB_ACTIVE_COLOR}
            inactiveTintColor={themeConstants[theme].TAB_INACTIVE_COLOR}
            style={{ backgroundColor: themeConstants[theme].TAB_BAR_COLOR }}
          />
        )}
      </ThemeContext.Consumer>
    );
  }
}
