import React, { PureComponent } from "react";
import {
  View,
  TouchableOpacity,
  Text,
  FlatList,
  StyleSheet
} from "react-native";

import store from "../../model/mobx/Store";
import { ThemeContext } from "../../../App";
import { themeConstants } from "../../res/themeConstants";

interface Props {
  data: string;
}
class TagList extends PureComponent<Props> {
  onPressTag = (text: string) => {
    store.searchText = text;
    store.searchPhotoByTag();
  };

  renderItem = ({ item }: any) => (
    <ThemeContext.Consumer>
      {theme => (
        <TouchableOpacity onPress={() => this.onPressTag(item)}>
          <Text style={[styles.textTag, {color: themeConstants[theme].SECOND_TEXT_COLOR}]}>#{item} </Text>
        </TouchableOpacity>
      )}
    </ThemeContext.Consumer>
  );

  render() {
    return (
      <View>
        <FlatList
          horizontal={true}
          data={this.props.data.split(" ")}
          renderItem={this.renderItem}
          keyExtractor={(item: string) => item}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  textTag: {
    marginVertical: 5,
    fontSize: 15
  }
});

export default TagList;
