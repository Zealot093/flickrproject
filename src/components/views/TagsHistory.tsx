import React, { Component } from "react";
import {
  View,
  FlatList,
  TouchableOpacity,
  Text,
  StyleSheet
} from "react-native";
import { observer } from "mobx-react";

import { iTagCache } from "../../model/interfaces";
import { ThemeContext } from "../../../App";
import { themeConstants } from "../../res/themeConstants";
import { toJS } from "mobx";

interface Props {
  onPressTag(tag: string): void;
  tags: string[];
}
@observer
class TagsHistory extends Component<Props> {
  _renderItem = ({ item }: any) => (
    <ThemeContext.Consumer>
      {theme => (
        <TouchableOpacity onPress={() => this.props.onPressTag(item)}>
          <Text style={[styles.tag, {color: themeConstants[theme].SECOND_TEXT_COLOR}]}>{item} </Text>
        </TouchableOpacity>
      )}
    </ThemeContext.Consumer>
  );

  render() {
    console.log(toJS(this.props.tags))
    let taglist;
    if (this.props.tags.length != 0) {
      taglist = (
        <ThemeContext.Consumer>
          {theme => (
            <View
              style={[
                styles.container,
                { backgroundColor: themeConstants[theme].SECOND_BACKGROUND_COLOR }
              ]}
            >
              <FlatList
                horizontal={true}
                data={this.props.tags}
                renderItem={this._renderItem}
                keyExtractor={(item: string) => item}
                showsHorizontalScrollIndicator={false}
              />
            </View>
          )}
        </ThemeContext.Consumer>
      );
    }
    return <View>{taglist}</View>;
  }
}

const styles = StyleSheet.create({
  container: {
    padding: 12,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,

    elevation: 5
  },
  tag: {
    borderWidth: 1,
    paddingHorizontal: 3,
    marginHorizontal: 3,
    borderRadius: 7,
    textAlign: "center",
    paddingLeft: 5,
    borderColor: "#757575"
  }
});

export default TagsHistory;
