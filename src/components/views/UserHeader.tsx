import React, { PureComponent } from "react";
import { StyleSheet } from "react-native";
import { Header, Item, Input, Button, Right } from "native-base";
import Icon from "react-native-vector-icons/FontAwesome";
import { observer } from "mobx-react";

import store from "../../model/mobx/Store";

interface Props {
}
@observer
class UserHeader extends PureComponent<Props> {
  render() {
    return (
      <Header searchBar rounded hasSegment androidStatusBarColor="#181818" style={{backgroundColor: "#262629"}}> 
        <Item>
          <Icon name="search" size={20} style={styles.iconSearch} />
          <Input
            placeholder="Search by username"
            onChangeText={store.handleUserSearchText}
            value={store.userSearchText}
            onSubmitEditing={() => store.searchPhotoByUsername()}
          />
        </Item>
      </Header>
    );
  }
}

const styles = StyleSheet.create({
  iconSearch: {
    marginHorizontal: 10,
  },
})

export default UserHeader