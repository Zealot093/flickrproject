import React, { PureComponent } from "react";
import { observer } from "mobx-react";
import { View, Text, StyleSheet, TouchableOpacity } from "react-native";
import { Thumbnail } from "native-base";

import { urlAvatarAssemble } from "../../utils/urlAvatarAssemble";
import { ThemeContext } from "../../../App";
import { themeConstants } from "../../res/themeConstants";

interface Props {
  navigation?: any;
  name: string;
  userId: string;
  onPressName?(): void;
}
@observer
class UserName extends PureComponent<Props> {
  render() {
    return (
      <ThemeContext.Consumer>
        {theme => (
          <View
            style={[
              styles.container,
              { backgroundColor: themeConstants[theme].SECOND_BACKGROUND_COLOR }
            ]}
          >
            <Thumbnail source={{ uri: urlAvatarAssemble(this.props.userId) }} />
            <View style={styles.userData}>
              <TouchableOpacity onPress={this.props.onPressName}>
                <Text style={{ color: themeConstants[theme].MAIN_TEXT_COLOR }}>
                  {this.props.name}
                </Text>
              </TouchableOpacity>
              <Text style={{ fontSize: 12, color: themeConstants[theme].SECOND_TEXT_COLOR }}>
                {this.props.userId}
              </Text>
            </View>
          </View>
        )}
      </ThemeContext.Consumer>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flexDirection: "row",
    margin: 12
  },
  userData: {
    marginTop: 8,
    marginRight: 50,
    marginLeft: 10
  },
  userID: {
    fontSize: 12,
    color: "#808080"
  },
  userName: {
    color: "black"
  }
});

export default UserName;
