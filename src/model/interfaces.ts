export interface iImage {
  id: string;
  url_m: string;
  title: string;
  owner: string;
  ownername: string;
  datetaken: string;
  description: { _content: string };
  tags: string;
  secret: string;
  views: string;
}

export interface iComment {
  id: string;
  author: string;
  authorname: string;
  _content: string;
}

export interface iTagCache {
  created: string;
  value: string;
}

export interface iUser {
  id: string;
  nsid: string;
  username: { _content: string };
}
 
