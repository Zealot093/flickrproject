import { observable, action } from "mobx";
import { cache } from "../../utils/Cache";

class ApplicationStore {
  @observable nightThemeSwitch: boolean = false;
  @observable themeValue: string = "light";

  @action
  saveThemeToCache = () => {
    cache.setItem("theme", this.themeValue, (err: any) => {});
    console.log(cache);
  };

  @action
  changeTheme = () => {
    this.themeValue === "light"
      ? ((this.themeValue = "dark"), (this.nightThemeSwitch = true))
      : ((this.themeValue = "light"), (this.nightThemeSwitch = false));
    this.saveThemeToCache()  
  };

  @action
  getThemeFromCache = () => {
    cache.peekItem('theme', (err: any, value: string) => {
      value != undefined ? this.themeValue = value : this.themeValue = 'light'
      value === "light" ? this.nightThemeSwitch = false : this.nightThemeSwitch = true;
    })
  }

}

const appStore = new ApplicationStore();
export default appStore;
