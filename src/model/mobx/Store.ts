import { observable, action, toJS, computed } from "mobx";
import { cache } from "../../utils/Cache";

import {
  getImagesByTag,
  getImagesByUser,
  getUserByName,
  getCommentsByPhoto,
  getRecentImages
} from "../../API/getImages";
import { iImage, iComment, iTagCache, iUser } from "../interfaces";

class Store {
  @observable.shallow images: iImage[] = [];
  @observable userImages: iImage[] = [];
  @observable searchText: string = "";
  @observable userSearchText: string = "";
  @observable page: number = 1;
  @observable pageUserPhoto: number = 1;
  @observable comments: iComment[] = [];
  @observable user: any;
  @observable historyTags: string[] = [];
  @observable loading: boolean = false;
  tags: string[] = [];
  

  @action
  saveTagToCache() {
    this.getTagsFromCache()
    this.historyTags = this.uniqTags(this.historyTags, this.searchText)
    cache.setItem('tags', this.historyTags, function(err: any) {});
    this.getTagsFromCache()
  }

  uniqTags(array: string[], element: string) {
    array.push(element)
    return [ ...new Set(array)]
  }

  uniqBy(a: any[], key: any) {
    return [...new Map(a.map((x: any) => [key(x), x])).values()];
  }

  @computed
  get uniqueImages() {
    let uniqueImages: iImage[] = this.uniqBy(this.images, JSON.stringify);
    console.log(uniqueImages);
    return uniqueImages;
  }
  
  @action
  getTagsFromCache = () => {
    cache.peekItem('tags', (err: any, value: any) => {
      value != undefined ? this.historyTags = value : this.historyTags = []
    })
    console.log(toJS(this.historyTags))
  };

  @action("search by tag")
  searchPhotoByTag = async () => {
    this.page = 1;
    let imagesJson = await getImagesByTag(this.searchText, this.page);
    this.images = [...imagesJson];
    this.saveTagToCache();
    console.log(toJS(this.images));
  };

  @action
  searchRecentPhoto = async () => {
    this.page = 1;
    let imagesJson = await getRecentImages(this.page);
    this.images = [...imagesJson];
  };

  @action
  searchPhotoByUser = async (
    userID: string,
    username: string = this.userSearchText
  ) => {
    let user = await getUserByName(username);
    console.log(user);
    let imagesJson = await getImagesByUser(userID);
    this.user = user;
    console.log(this.user);
    this.userImages = [...imagesJson];
    console.log(toJS(this.userImages));
  };

  @action
  searchPhotoByUsername = async (username = this.userSearchText) => {
    console.log(username);
    let user = await getUserByName(username);
    let imagesJson = await getImagesByUser(user.id);
    this.user = user;
    console.log(user);
    this.userImages = [...imagesJson];
    console.log(toJS(this.userImages));
  };

  @action
  getComments = async (id: string) => {
    let commentsJson = await getCommentsByPhoto(id);
    if (commentsJson) {
      this.comments = [...commentsJson];
    }
    await console.log(commentsJson);
  };

  @action("more photos")
  handleLoadMore = async () => {
    this.page = this.page + 1;
    console.log(this.page);
    let imagesJson = this.searchText
      ? await getImagesByTag(this.searchText, this.page)
      : await getRecentImages(this.page);
    this.images = [...this.images, ...imagesJson];
  };

  @action
  handleLoadMoreUserPhoto = async (userID: string) => {
    this.pageUserPhoto = this.pageUserPhoto + 1;
    console.log(this.pageUserPhoto);
    let imagesJson = await getImagesByUser(userID, this.pageUserPhoto);
    this.userImages = [...this.userImages, ...imagesJson];
  };

  @action
  handleSearchText = (text: string) => {
    this.searchText = text;
  };

  @action
  handleUserSearchText = (text: string) => {
    this.userSearchText = text;
  };
}

const store = new Store();
export default store;
