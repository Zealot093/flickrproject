import { StyleSheet } from "react-native";
import * as colors from "./colors"

export const styles = StyleSheet.create({
  welcomeText: {
    height: 500,
    textAlignVertical: "center",
    textAlign: "center"
  },
  defaultScreenContainer: {
    backgroundColor: colors.MAIN_BACKGROUND_COLOR,
  },
});
