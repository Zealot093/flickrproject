
interface theme {
  [key: string]: {
    PRIMARY_COLOR: string;
    STATUS_BAR_COLOR: string;
    MAIN_TEXT_COLOR: string;
    SECOND_TEXT_COLOR: string;
    MAIN_BACKGROUND_COLOR: string;
    SECOND_BACKGROUND_COLOR: string;
    MAIN_ICON_COLOR: string;
    SECOND_ICON_COLOR: string;
    TAB_BAR_COLOR: string;
    TAB_ACTIVE_COLOR: string;
    TAB_INACTIVE_COLOR: string;
  }
}

export const themeConstants: theme = {
  light: {
    PRIMARY_COLOR: "#262629",
    STATUS_BAR_COLOR: "#181818",
    MAIN_TEXT_COLOR: "black",
    SECOND_TEXT_COLOR: "grey",
    MAIN_BACKGROUND_COLOR: "#f2f1f0",
    SECOND_BACKGROUND_COLOR: "white",
    MAIN_ICON_COLOR: "grey",
    SECOND_ICON_COLOR: "white",
    TAB_BAR_COLOR: "white",
    TAB_ACTIVE_COLOR: "#262629",
    TAB_INACTIVE_COLOR: "grey"
  },
  dark: {
    PRIMARY_COLOR: "#262629",
    STATUS_BAR_COLOR: "#181818",
    MAIN_TEXT_COLOR: "white",
    SECOND_TEXT_COLOR: "#cfcfcf",
    MAIN_BACKGROUND_COLOR: "black",
    SECOND_BACKGROUND_COLOR: "#46464d",
    MAIN_ICON_COLOR: "grey",
    SECOND_ICON_COLOR: "white",
    TAB_BAR_COLOR: "#262629",
    TAB_ACTIVE_COLOR: "white",
    TAB_INACTIVE_COLOR: "#4a4a4a",
  }
};
