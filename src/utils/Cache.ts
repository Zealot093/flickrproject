import { Cache } from "react-native-cache";
import AsyncStorage from "@react-native-community/async-storage";

export let cache = new Cache({
  namespace: "myapp",
  policy: {
      maxEntries: 20
  },
  backend: AsyncStorage
});