import RNFetchBlob from "rn-fetch-blob";
import { PermissionsAndroid, Alert } from "react-native";

export  const downloadPhoto = (url: string) =>{
  console.log(url);
  let dirs = RNFetchBlob.fs.dirs;
  RNFetchBlob.config({
    addAndroidDownloads: {
      useDownloadManager: true,
      notification: true,
      path: dirs.PictureDir + "/" + Date.now() + ".png",
      mime: "image/png",
      description: "File downloaded by download manager."
    }
  })
    .fetch("GET", url)
    .then(resp => {
      console.log(resp.path());
      resp.path();
    });
}

export const downloadFile = async (url: string) => {
  try {
    const granted = await PermissionsAndroid.request(
      PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
      {
        title: "Storage Permission",
        message: "App needs access to memory to download the file ",
        buttonNeutral: "Ask Me Later",
        buttonNegative: "Cancel",
        buttonPositive: "OK"
      }
    );
    if (granted === PermissionsAndroid.RESULTS.GRANTED) {
      downloadPhoto(url);
    } else {
      Alert.alert(
        "Permission Denied!",
        "You need to give storage permission to download the file"
      );
    }
  } catch (err) {
    console.warn(err);
  }
}
