import store from "../model/mobx/Store";

export const goToUser = (userId: string, name: string, navigation: any) => {
  console.log("GO")
  store.handleUserSearchText(name)
  if (navigation) {
    navigation.navigate("UserScreen", {
      name: name,
      userId: userId
    });
    store.searchPhotoByUser(userId, name);
  }
};