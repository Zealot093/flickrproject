export const urlAvatarAssemble = (id: string) =>
  `https://live.staticflickr.com/1933/buddyicons/${id}.jpg`;
